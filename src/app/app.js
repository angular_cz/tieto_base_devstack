(function () {
  'use strict';

  angular
    .module('cz.angular.training', [
      'cz.angular.training.hello',
      'ngRoute'
    ])
    .config(function ($routeProvider) {
        $routeProvider

          .when('/', {
            controller: 'HelloController',
            controllerAs: 'hello',
            templateUrl: 'hello/hello.html'
          })
          
          .otherwise('/');
      });
})();