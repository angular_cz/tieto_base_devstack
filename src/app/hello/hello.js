(function() {
  'use strict';

  angular
      .module('cz.angular.training.hello', [])
      .controller('HelloController', function($http) {
        $http.get('/api/beer')
            .then(function(response) {
              this.data = response.data;
            }.bind(this));
      });
})();