# Angular.cz devstack #

Devstack for simple start with AngularJS by Angular.cz

### Using ###

* Clone repository
* run 
```
npm install
gulp devel
```

### Gulp tasks ###

**devel**

* run application from source codes, run build-in development server with watching and livereload

**build**

* make minified application in build directory

Testing

```
# karma
npm run test

# protractor

npm run update-webdriver
# pouze před prvním spuštěním protraktoru

npm run protractor
```

### API - Beer ###
```
# Seznam piv
# jednoduché stránkování
GET - http://api-angularcz.rhcloud.com/beer-api/beer
GET - http://api-angularcz.rhcloud.com/beer-api/beer?page=1

[ {...}, {...}]

# stránkování 
GET - http://api-angularcz.rhcloud.com/beer-api/beer/pagable

{
	content: [ {...}, {...}],
	first: false,
	firstPage: false,
	last: false,
	lastPage: false,
	number: 2,
	numberOfElements: 20,
	size: 20,
	sort: [{direction: "ASC", property: "name", ignoreCase: true, nullHandling: "NATIVE", ascending: true}],
	totalElements: 313,
	totalPages: 16
}

# získání jednoho (detailu)

GET - http://api-angularcz.rhcloud.com/beer-api/beer/1

{
	degree: "Nealko",
	description: "světlé pivo",
	id: 1,
	name: "Bakalář",
	brewery: {id: 1, name: "Benešov"},
}

# Vytvoření
POST - http://api-angularcz.rhcloud.com/beer-api/beer

{
	degree: "Nealko",
	description: "světlé pivo",
	id: 1,
	name: "Bakalář",
	breweryId: 1
}

# Editace
POST - http://api-angularcz.rhcloud.com/beer-api/beer/1

{
	degree: "Nealko",
	description: "světlé pivo",
	id: 1,
	name: "Bakalář",
	breweryId: 1
}

```

Security API
```

POST 	http://api-angularcz.rhcloud.com/security-api/login
GET 	http://api-angularcz.rhcloud.com/security-api/logout

GET 	http://api-angularcz.rhcloud.com/security-api/orders
POST 	http://api-angularcz.rhcloud.com/security-api/orders

GET	        http://api-angularcz.rhcloud.com/security-api/orders/1
POST	http://api-angularcz.rhcloud.com/security-api/orders/1
```